
(function(){

  // Inicia o firebase Firebase
  var config = {
    apiKey: "AIzaSyD6kajvcmBb3RPW8vvukeAVk64x6U3JnKE",
    authDomain: "mrdvminh-3f442.firebaseapp.com",
    databaseURL: "https://bionicgarden-afbdc.firebaseio.com",
    projectId: "mrdvminh-3f442",
    storageBucket: "mrdvminh-3f442.appspot.com",
    messagingSenderId: "21420499631"
  };
  firebase.initializeApp(config);

  var db = firebase.database();

  // Creates the data listeners in the firebase
  var tempRef = db.ref('bionic/device/0/sensor/tempinair');
  var umidRef = db.ref('bionic/device/0/sensor/humidity');
  var tempinwaterRef = db.ref('bionic/device/0/sensor/tempinwater');
  var tdsRef = db.ref('bionic/device/0/sensor/tds');
  var lightRef = db.ref('bionic/device/0/sensor/light');
  var lampRef = db.ref('bionic/device/0/switch/led');
  var pumpRef = db.ref('bionic/device/0/switch/pump');


  // Registers functions that updates current telemetry charts and data
  /*tempRef.on('value', onNewData('currentTemp', 'tempLineChart' , '°C', '°C'));
  umidRef.on('value', onNewData('currentUmid', 'umidLineChart' , '%', '%'));
  tempinwaterRef.on('value', onNewData('currentTempWater', 'tempwaterLineChart' , '°C', '°C'));
  tdsRef.on('value', onNewData('currenTds', 'tdsLineChart' , 'ppm', 'ppm'));
  lightRef.on('value', onNewData('currenlight', 'lightLineChart' , 'lux', 'lux'));*/
  
  // Register function when changing lamp value
  var currentLampValue;
  lampRef.on('value', function(snapshot){
    var value = snapshot.val();
    var el = document.getElementById('currentLamp');
    if(value){
      el.classList.remove('amber-text');
    }else{
      el.classList.add('amber-text');
    }
    currentLampValue = value;
  });

  // Register function click on lamp button
  var btnLamp = document.getElementById('btn-lamp');
  btnLamp.addEventListener('click', function(evt){
    lampRef.set(!currentLampValue);
  });

  //pump
  var currenPumpValue;
  pumpRef.on('value', function(snapshot){
    var value = snapshot.val();
   if(value){
      document.getElementById('btn-pump').innerText = 'OFF';
   }
   else{
      document.getElementById('btn-pump').innerText = 'ON';
   }
    currentPumpValue = value;
  });

  // Register function click on pump button
  var btnPump = document.getElementById('btn-pump');
  btnPump.addEventListener('click', function(evt){
    pumpRef.set(!currentPumpValue);
  });

  //get temp in air
  tempRef.on('value', function(snapshot){
    var value = snapshot.val();
    document.getElementById('currentTemp').innerText = value + ' ' + '°C';
  });
  //get temp in water
  tempinwaterRef.on('value', function(snapshot){
    var value = snapshot.val();
    document.getElementById('currentTempWater').innerText = value + ' ' + '°C';
  });

  //get humidity
  umidRef.on('value', function(snapshot){
    var value = snapshot.val();
    document.getElementById('currentUmid').innerText = value + ' ' + '%';
  });

  //get light
  lightRef.on('value', function(snapshot){
    var value = snapshot.val();
    document.getElementById('currenlight').innerText = value + ' ' + 'lux';
  });

  //get tds
  tdsRef.on('value', function(snapshot){
    var value = snapshot.val();
    document.getElementById('currenTds').innerText = value;
  });

  /*//Get element
  const txtEmail = document.getElementById('txtEmail');
  const txtPassword = document.getElementById('txtPassword');
  const btnLogin = document.getElementById('btnLogin');
  const btnLogout = document.getElementById('btnLogout');
  const btnSignup = document.getElementById('btnSignup');

//Add login event
btnLogin.addEventListener('click', e=> {
  //get Email and Password
  const email = txtEmail.value;
  const pass = txtPassword.value;
  const auth = firebase.auth();
  //Sign in
  const promise = auth.signInWithEmailAndPassword(email, pass);
  promise.catch(e => console.log(e.message));
  if(email=='mrdvminh@gmail.com'){
    window.location = "device.html";
  }
  else if(email=='admin@gmail.com'){
    window.location = "admin.html";
  }
});

//Add signup event
btnSignup.addEventListener('click', e=> {
  //Get email and password
  const email = txtEmail.value;
  const pass = txtPassword.value;
  const auth = firebase.auth();
  //SignUp
  const promise = auth.createUserWithEmailAndPassword(email, pass);
  promise
  .then(user => console.log(user))
  .catch(e => console.log(e.message));
});

//Logout
btnLogout.addEventListener('click', e=> {
  Firebase.auth().signOut();
});

//Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
  if(firebaseUser){
    console.log(firebaseUser);
  }
  else{
    console.log('not logged in');
  }
});*/

})();


// Returns a function that according to data changes
// Updates the current value of the element, with the last metric (currentValueEl and metric)
// and mounts the graph with the data and description of the data type (chartEl, label)
function onNewData(currentValueEl, chartEl, label, metric){
  return function(snapshot){
    var readings = snapshot.val();
    if(readings){
      var currentValue;
      var data = [];
      for(var key in readings){
        currentValue = readings[key]
        data.push(currentValue);
      }

      document.getElementById(currentValueEl).innerText = currentValue + ' ' + metric;
      buildLineChart(chartEl, label, data);
    }
  }
}

// Construct a line graph in the element (el) with the description (label) and the
// past data (date)
function buildLineChart(el, label, data){
  var elNode = document.getElementById(el);
  new Chart(elNode, {
    type: 'line',
    data: {
      labels: new Array(data.length).fill(""),
      datasets: [{
        label: label,
        data: data,
        borderWidth: 1,
        fill: false,
        spanGaps: false,
        lineTension: 0.1,
        backgroundColor: "#F9A825",
        borderColor: "#F9A825"
      }]
    }
  });
}


