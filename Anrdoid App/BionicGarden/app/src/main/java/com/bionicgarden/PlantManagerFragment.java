package com.bionicgarden;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.bionicgarden.Adapter.PlantAdapter;
import com.bionicgarden.Model.Plant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samvuduc on 6/12/17.
 */

public class PlantManagerFragment extends Fragment {
    public PlantManagerFragment() {
    }


    private TextView macIp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.plant_listview, container, false);
        ListView listView = (ListView) view.findViewById(R.id.lv_Plant);
        List<Plant> allItems = data();
        PlantAdapter mAdapter = new PlantAdapter(getActivity(), allItems);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });


        FloatingActionButton add = (FloatingActionButton) view.findViewById(R.id.addPlant);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ConnectModuleActivity.class);
                startActivity(intent);

            }
        });
        return view;

    }

    private List<Plant> data() {
        List<Plant> items = new ArrayList<>();
        items.add(new Plant("gardent", "Connecting", "normal", "normal", R.drawable.plants));
        return items;
    }
}
