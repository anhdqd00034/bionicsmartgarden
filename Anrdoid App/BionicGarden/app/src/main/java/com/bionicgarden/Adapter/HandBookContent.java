package com.bionicgarden.Adapter;

import com.bionicgarden.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HandBookContent {


    public static final List<HandbookItem> ITEMS = new ArrayList<HandbookItem>();


    public static final Map<String, HandbookItem> ITEM_MAP = new HashMap<String, HandbookItem>();

    static {
        addItem(new HandbookItem("1", Integer.toString(R.drawable.coriander), "Coriander", R.string.Coriander));
        addItem(new HandbookItem("2", Integer.toString(R.drawable.centella), "Centella", R.string.Centella));
        addItem(new HandbookItem("3", Integer.toString(R.drawable.watercress), "WaterCress", R.string.Watercress));
        addItem(new HandbookItem("4", Integer.toString(R.drawable.oregano), "Basil", R.string.Basil));
        addItem(new HandbookItem("5", Integer.toString(R.drawable.oregano), "Oregano", R.string.Oregano));
        addItem(new HandbookItem("6", Integer.toString(R.drawable.lectures), "Lecture", R.string.Lecture));
        addItem(new HandbookItem("7", Integer.toString(R.drawable.radish), "Radish", R.string.Radish));
        addItem(new HandbookItem("8", Integer.toString(R.drawable.carot), "Carrot", R.string.Carrot));
        addItem(new HandbookItem("9", Integer.toString(R.drawable.spinach), "Spinash", R.string.Spinash));

    }

    private static void addItem(HandbookItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    public static class HandbookItem {
        public final String id;
        public String imgPlant;
        public final String content;
        public final int details;

        public HandbookItem(String id, String imgPlant, String content, int details) {
            this.id = id;
            this.imgPlant = imgPlant;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}

