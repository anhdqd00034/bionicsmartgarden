package com.bionicgarden;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by samvuduc on 6/15/17.
 */

public class HandbookFragment extends Fragment {

    public HandbookFragment() {
    }


    @Override
    public  View onCreateView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_handbook_fragment, container, false);

        Button goHandbook = (Button) view.findViewById(R.id.handbookGo);
        goHandbook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), HandbookListActivity.class );
                        startActivity(intent);
            }
        });
        return view;


    }

}
