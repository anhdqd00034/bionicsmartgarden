package com.bionicgarden;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingModuleActivity extends AppCompatActivity {
    DatabaseReference mData;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    @BindView(R.id.rad800)
    RadioButton rad800;
    @BindView(R.id.rad1100)
    RadioButton rad1100;
    @BindView(R.id.rad1500)
    RadioButton rad1500;
    @BindView(R.id.radOnPump)
    RadioButton radOnPump;
    @BindView(R.id.radOffPump)
    RadioButton radOffPump;
    @BindView(R.id.radOnLight)
    RadioButton radOnLight;
    @BindView(R.id.radOffLight)
    RadioButton radOffLight;
    @BindView(R.id.btnsetting)
    Button btnsetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_sensor);
        ButterKnife.bind(this);
        settingFirebae();
    }

    private void settingFirebae() {
        btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mData = database.getReference();
                String s = "";
                String a = "";
                String b = "";
                //chọn giá trị nutri
                if (rad800.isChecked()) {
                    s = "800";
                } else if (rad1100.isChecked()) {
                    s = "1100";
                } else if (rad1500.isChecked()) {
                    s = "1500";
                }
                //chọn bật tắt máy bơm
                if (radOnPump.isChecked()) {
                    a = "true";
                } else if (radOffPump.isChecked()) {
                    a = "false";
                }
                //chọn bật tắt đèn
                if (radOnLight.isChecked()) {
                    b = "true";
                } else if (radOffLight.isChecked()) {
                    b = "false";
                }
                mData.child("bionic").child("device").child("0").child("setting").child("nutri").setValue(s, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Toast.makeText(SettingModuleActivity.this, "nutri ok", Toast.LENGTH_SHORT).show();
                    }
                });
                mData.child("bionic").child("device").child("0").child("switch").child("pump").setValue(a, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Toast.makeText(SettingModuleActivity.this, "Pump ok", Toast.LENGTH_SHORT).show();
                    }
                });
                mData.child("bionic").child("device").child("0").child("switch").child("led").setValue(b, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        Toast.makeText(SettingModuleActivity.this, "light ok", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
