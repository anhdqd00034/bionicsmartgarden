package com.bionicgarden.Model;

/**
 * Created by samvuduc on 6/12/17.
 */

public class Plant {

    private String nameGarden;
    private String connectStatus;
    private String plantStatus;
    private String sensorStatus;
    private int imagePlant;

    public Plant() {
    }

    public Plant(String nameGarden, String connectStatus, String plantStatus, String sensorStatus, int imagePlant) {
        this.nameGarden = nameGarden;
        this.connectStatus = connectStatus;
        this.plantStatus = plantStatus;
        this.sensorStatus = sensorStatus;
        this.imagePlant = imagePlant;
    }

    public String getNameGarden() {
        return nameGarden;
    }

    public void setNameGarden(String nameGarden) {
        this.nameGarden = nameGarden;
    }

    public String getConnectStatus() {
        return connectStatus;
    }

    public void setConnectStatus(String connectStatus) {
        this.connectStatus = connectStatus;
    }

    public String getPlantStatus() {
        return plantStatus;
    }

    public void setPlantStatus(String plantStatus) {
        this.plantStatus = plantStatus;
    }

    public String getSensorStatus() {
        return sensorStatus;
    }

    public void setSensorStatus(String sensorStatus) {
        this.sensorStatus = sensorStatus;
    }

    public int getImagePlant() {
        return imagePlant;
    }

    public void setImagePlant(int imagePlant) {
        this.imagePlant = imagePlant;
    }

}
