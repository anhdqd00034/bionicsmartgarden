package com.bionicgarden.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bionicgarden.PlantManager;
import com.bionicgarden.PlantManagerFragment;
import com.bionicgarden.R;
import com.bionicgarden.Model.Plant;

import java.util.List;

/**
 * Created by samvuduc on 6/12/17.
 */

public class PlantAdapter extends BaseAdapter {
    private Activity context;
    private List<Plant> listPlantGarden;

    public PlantAdapter(Activity context, List<Plant> customizedListView) {
        this.context = context;
        listPlantGarden = customizedListView;
    }

    @Override
    public int getCount() {
        return listPlantGarden.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (context).getLayoutInflater();
            rowView = inflater.inflate(R.layout.content_plant_manager, null);
            ViewHolder holder = new ViewHolder();
            holder.content_plant_manager = rowView.findViewById(R.id.myPlant);
            holder.tv_connectStatus = (TextView) rowView.findViewById(R.id.tv_connectStatus);
            holder.tv_plantStatus = (TextView) rowView.findViewById(R.id.tv_plantStatus);
            holder.tv_sensorStatus = (TextView) rowView.findViewById(R.id.tv_sensorStatus);
            holder.imgPlant = (ImageView) rowView.findViewById(R.id.imgPlant);

            rowView.setTag(holder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_connectStatus.setText(listPlantGarden.get(position).getConnectStatus());
        holder.tv_plantStatus.setText(listPlantGarden.get(position).getPlantStatus());
        holder.tv_sensorStatus.setText(listPlantGarden.get(position).getSensorStatus());

        holder.imgPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context  ,PlantManager.class);
                context.startActivity(intent);            }
        });

        return rowView;
    }



    private class ViewHolder {
        View content_plant_manager;
        ImageView imgPlant;
        TextView tv_connectStatus, tv_sensorStatus, tv_plantStatus;

    }


}
