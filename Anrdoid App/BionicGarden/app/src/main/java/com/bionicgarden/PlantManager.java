package com.bionicgarden;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.BooleanResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
/**
 * Created by samvuduc on 6/12/17.
 */
public class PlantManager extends FragmentActivity {
    DatabaseReference mData;
    FirebaseDatabase database=FirebaseDatabase.getInstance();
    TextView lbl_Hum, lbl_numLight, lbl_numTds, lbl_numLevelW, lbl_numTemPW, lbl_numTempIn, lbl_statusLight,lbl_statuslevelWater;
    TextView mac;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_detail);
        addControls();
        addFireBase();

        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        toolbar.setTitle(getString(R.string.sensor));
        // Set activity change to setting layout
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addDevice);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PlantManager.this,SettingModuleActivity.class);
                startActivity(intent);
            }
        });
    }
    private void addControls() {
        lbl_Hum = (TextView) findViewById(R.id.lbl_numHum);
        lbl_numLight = (TextView) findViewById(R.id.lbl_numLight);
        lbl_numTds = (TextView) findViewById(R.id.lbl_numTds);
        lbl_numLevelW = (TextView) findViewById(R.id.lbl_numLevelW);
        lbl_numTempIn = (TextView) findViewById(R.id.lbl_numTempIn);
        lbl_numTemPW = (TextView) findViewById(R.id.lbl_numTemPW);
        lbl_statusLight = (TextView) findViewById(R.id.lbl_statusLight);
        lbl_statuslevelWater = (TextView) findViewById(R.id.lbl_statuslevelWater);
    }
    private void addFireBase() {
        mData =database.getReference();
        mData.child("bionic").child("device").child("0").child("sensor").child("humidity").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_Hum.setText(dataSnapshot.getValue().toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        mData.child("bionic").child("device").child("0").child("sensor").child("light").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_numLight.setText(dataSnapshot.getValue().toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        mData.child("bionic").child("device").child("0").child("sensor").child("tds").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_numTds.setText(dataSnapshot.getValue().toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        mData.child("bionic").child("device").child("0").child("sensor").child("tempinair").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_numTempIn.setText(dataSnapshot.getValue().toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        mData.child("bionic").child("device").child("0").child("sensor").child("tempinwater").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_numTemPW.setText(dataSnapshot.getValue().toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        mData.child("bionic").child("device").child("0").child("sensor").child("waterlevel").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_numLevelW.setText(dataSnapshot.getValue().toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        mData.child("bionic").child("device").child("0").child("switch").child("pump").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_statusLight.setText(dataSnapshot.getValue().toString());
                if(lbl_statusLight.equals("false")){
                    lbl_statusLight.setText("On");
                }
                else {
                    lbl_statusLight.setText("Off");
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        mData.child("bionic").child("device").child("0").child("switch").child("led").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lbl_statuslevelWater.setText(dataSnapshot.getValue().toString());
                if(lbl_statuslevelWater.equals("false")){
                    lbl_statuslevelWater.setText("On");
                }
                else {
                    lbl_statuslevelWater.setText("Off");
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, PlantManager.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}