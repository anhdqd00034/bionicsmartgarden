package com.bionicgarden.Model;

/**
 * Created by samvuduc on 6/12/17.
 */

public class HandbookItem {

    private int imgPlant;
    private String plantName;
    private String descripton;

    public HandbookItem(int imgPlant, String plantName, String descripton) {
        this.imgPlant = imgPlant;
        this.plantName = plantName;
        this.descripton = descripton;
    }

    public int getImgPlant() {
        return imgPlant;
    }

    public String getPlantName() {
        return plantName;
    }
    public String getDescripton(){return descripton; }
}
