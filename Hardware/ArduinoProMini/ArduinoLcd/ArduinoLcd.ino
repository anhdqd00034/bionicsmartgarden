#include <SoftwareSerial.h>
#include <SerialCommand.h>  
#include <ArduinoJson.h>
 //LCD ST7565 128x64
#include "ST7565_homephone.h"
#include <bmps.h>

ST7565 lcd(3,4,5,6,7);
// virtual serial port
const byte RX = 9;
const byte TX = 8;
SoftwareSerial mySerial(RX,TX); // RX, TX
SerialCommand sCmd(mySerial);

int temp_water;
int ppm;
int lux;
int temp_inair;
int humi_inair;
int waterlevel;

void setup() {
 Serial.begin(9600);
 mySerial.begin(9600);
 sCmd.addCommand("SENTDATA", getDataArduino);
  // sCmd.addDefaultHandler(getDataArduino);
  lcd.ON();
  lcd.SET(22,0,0,0,4); 
 displaylogo(30);
}

void loop() {
     sCmd.readSerial();
     lcd.Plus_Bitmap( 0,5,24,24, temp,0,0,BLACK);
     lcd.Number_Long(24, 15,temp_inair,ASCII_NUMBER,BLACK);
     lcd.asc_char(37,13,248, BLACK);
     lcd.asc_char(44,15,67, BLACK);

       
     lcd.Plus_Bitmap( 60,5,24,24, humidity,0,0,BLACK);
     lcd.Number_Long(90,15,humi_inair,ASCII_NUMBER,BLACK);
     lcd.asc_char(105,15,37, BLACK);
     
     lcd.Plus_Bitmap( 0,36,24,24, temp,0,0,BLACK); 
     lcd.Number_Long(24, 45,temp_water,ASCII_NUMBER,BLACK);
     lcd.asc_char(37,42,248, BLACK);
     lcd.asc_char(44,45,67, BLACK);
      
     lcd.Plus_Bitmap( 60,36,24,24, light_lux,0,0,BLACK);
     lcd.Number_Long(90,45,lux,ASCII_NUMBER,BLACK);
     lcd.display();
     delay(1000);


     
     }
void displaylogo(int OnTime){ 
    int var = 0;
     while(var < OnTime){
     lcd.Plus_Bitmap( 48,7,32,32,logoleaf,0,0,BLACK);
     const static unsigned char text[] PROGMEM ="BIONIC GARDEN";
     lcd.Asc_String(20,48,text, BLACK);
     lcd.display();
     lcd.Clear();
     var++;
     }
    }
  
  void getDataArduino(){
     #ifdef PROGMEM
  char *json = sCmd.next(); 
  Serial.println(json);
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(json);
  JsonArray& nestedArray = root[F("data")];
    JsonArray& nestedArray2 = root[F("set")];
      lux = (int)nestedArray[0];         // light sensor
      ppm = (int)nestedArray[1];         // tds sensor
      temp_water=(int) nestedArray[2]; // temp in water
      temp_inair = (int)nestedArray[3] ; // temp in air
      humi_inair = (int) nestedArray[4]; // humidity in air
      waterlevel= (int)nestedArray[5]; // water level
       #else
    #warning PROGMEM is not supported on this platform
    #endif
    }
