/*Copyright Dmitry Ivan 2017

MIT License
The MIT License (MIT)

Copyright (c) Dmitry Ivan <dmitryivan01@mail.ru> 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE
*/
 #include <Scheduler.h>
#include <ArduinoJson.h>
#include <avr/pgmspace.h>
#include <Wire.h>
#include <BH1750.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SoftwareSerial.h>
#include <SerialCommand.h>  
#include "DHT.h"
// virtual serial port
const byte RX = 13;
const byte TX = 12;
SoftwareSerial mySerial(RX, TX); // RX, TX
SerialCommand sCmd(mySerial);

// BH1750 Light Sensor
BH1750 lightMeter(0x23);
//Utrasonic Sensor HC-SR04
#define TRIGGER 9 // Pin 9 --> Trigger  
#define ECHO    8 // Pin 8 --> Echo
// DHT 11 Sensor Temp ,Humidity
#define DHTTYPE DHT11
#define DHTPIN 11 // Pin 11
// Initialize DHT sensor.
DHT dht(DHTPIN, DHTTYPE);

// Motor Speed & Direction
#define MOTOR_A_PWM  6 // Motor A
#define MOTOR_B_PWM 7 // Motor B

// Nutri level Sensor
#define nutria 3   // Pin 3 
#define nutrib 4   // Pin 4
#define swater 5

// Tds Sensor 
#define ONE_WIRE_BUS 10          // Data wire For Temp Probe is plugged into pin 12 on the Arduino
int R1= 1000;
int Ra=25; //Resistance of powering Pins
int ECPin= A0;
int ECGround= A1 ; //Pin A1 Input
int ECPower = A2 ; //Pin A2 Input
//*********** Converting to ppm [Learn to use EC it is much better**************//
// Hana      [USA]        PPMconverion:  0.5
// Eutech    [EU]          PPMconversion:  0.64
//Tranchen  [Australia]  PPMconversion:  0.7
float PPMconversion=0.7;
//*************Compensating for temperature ************************************//
//The value below will change depending on what chemical solution we are measuring
//0.019 is generaly considered the standard for plant nutrients [google "Temperature compensation EC" for more info
float TemperatureCoef = 0.019; //this changes depending on what chemical we are measuring
//********************** Cell Constant For Ec Measurements *********************//
//Mine was around 2.9 with plugs being a standard size they should all be around the same
//But If you get bad readings you can use the calibration script and fluid to get a better estimate for K
float K = 1.02;
//************ Temp Probe Related *********************************************//

OneWire oneWire(ONE_WIRE_BUS);// Setup a oneWire instance to communicate with any OneWire devices
DallasTemperature sensors(&oneWire);// Pass our oneWire reference to Dallas Temperature.
float Temperature=10;
float EC=0;
float EC25 =0;
int ppm =0;
float raw= 0;
float Vin= 5;
float Vdrop= 0;
float Rc= 0;
float buffer=0;
bool nutrilevel ;
int limitNutri = 1100; //Set limit Nutri

volatile float duration, distance ,percent;
uint16_t lux  ;
float temp_inair;
float humidity;
unsigned long lastmillis = 0;


void setup() { 
  Serial.begin(9600);
   // Open port
   mySerial.begin(9600);
  // sCmd.addCommand("SENTDATA", getData);
  dht.begin(); //dht11 sensor
  lightMeter.begin(BH1750_CONTINUOUS_HIGH_RES_MODE); // light senor
  pinMode(TRIGGER, OUTPUT);//utrasonic
  pinMode(ECHO, INPUT);
  //Pump Nutri
  pinMode( MOTOR_A_PWM, OUTPUT );
  pinMode( MOTOR_B_PWM, OUTPUT );
  digitalWrite( MOTOR_A_PWM, HIGH );
  digitalWrite( MOTOR_B_PWM, HIGH );
  //Nutri Level
  pinMode(nutria,INPUT);
  pinMode(nutrib,INPUT);
  //Swater
   pinMode(swater,OUTPUT);
   pinMode(swater,LOW);
  // Tds Sensor
  pinMode(ECPin,INPUT);
  pinMode(ECPower,OUTPUT);//Setting pin for sourcing current
  pinMode(ECGround,OUTPUT);//setting pin for sinking current
  digitalWrite(ECGround,LOW);//We can leave the ground connected permanantly
  delay(100);// gives sensor time to settle
  sensors.begin();
  delay(100);
  //** Adding Digital Pin Resistance to [25 ohm] to the static Resistor *********//
  // Consule Read-Me for Why, or just accept it as true
   R1=(R1+Ra);// Taking into acount Powering Pin Resitance
  
}


void loop() {
  sCmd.readSerial();
   getData();
  lux= lightMeter.readLightLevel();
  waterLevel();
  readDHT11();
  pushData();
  nutriLevel();
  nutriControl();
  getTdsEc();
}
//***************************************
//           DHT 11 sensor 
//***************************************
void readDHT11(){
humidity = dht.readHumidity();
// Read temperature as Celsius (the default)
temp_inair = dht.readTemperature();
// Read temperature as Fahrenheit (isFahrenheit = true)
float f = dht.readTemperature(true);
 // Check if any reads failed and exit early (to try again).
  if (isnan(humidity) || isnan(temp_inair) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
}

/*************************************************************
 *                 Tds Sensor
 ************************************************************/

void getTdsEc(){
//Reading Temperature Of Solution
sensors.requestTemperatures();// Send the command to get temperatures
Temperature=sensors.getTempCByIndex(0); //Stores Value in Variable 
//Estimates Resistance of Liquid
digitalWrite(ECPower,HIGH);
raw= analogRead(ECPin);
raw= analogRead(ECPin);// This is not a mistake, First reading will be low beause if charged a capacitor
digitalWrite(ECPower,LOW);
//***************** Converts to EC **************************//
Vdrop= (Vin*raw)/1024.0;
Rc=(Vdrop*R1)/(Vin-Vdrop);
Rc=Rc-Ra; //acounting for Digital Pin Resitance
EC = 1000/(Rc*K);
//*************Compensating For Temperaure********************//
EC25  =  EC/ (1+ TemperatureCoef*(Temperature-25.0));
ppm=(EC25)*(PPMconversion*1000);
  }
void pushData(){
  if((millis() - lastmillis) > 5000){
    lastmillis = millis();
     #ifdef PROGMEM
   DynamicJsonBuffer jsonBuffer;
   JsonObject& root = jsonBuffer.createObject();
   JsonArray& data = root.createNestedArray(F("data"));
    JsonArray& data2 = root.createNestedArray(F("set"));
   data.add(lux); 
   data.add(ppm);
   data.add((int)Temperature);
   data.add((int)temp_inair);
   data.add((int)humidity);
   data.add((int)percent);               
   data.add(nutrilevel); //nutri A
    data2.add(limitNutri);
    mySerial.print(F("SENTDATA"));
    mySerial.print('\r');
    root.printTo(mySerial);
    mySerial.print('\r');
    //Debug   
    Serial.print(F("SENTDATA"));
    Serial.print('\r');
    root.printTo(Serial);
    Serial.print('\r');
      #else
    #warning PROGMEM is not supported on this platform
    #endif
  }
  }

  void getData(){
     #ifdef PROGMEM
    char *json = sCmd.next(); 
     DynamicJsonBuffer jsonBuffer;
   // Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
     swaterCheck();
     digitalWrite(swater,root[F("swater")] ? HIGH : LOW);//  Switch Wate
     //root.printTo(Serial);
     #else
    #warning PROGMEM is not supported on this platform
    #endif
   }

/*************************************************************
 *                     Switch Water
 ************************************************************/  
  void swaterCheck(){
    if(percent >95){
      digitalWrite(swater,LOW);
      //Serial.print("Stop pump !! Water full");
      }
      yield();
    }
  
 
/*************************************************************
 *                      Pump Nutri
 ************************************************************/
 void nutriControl(){
  if((millis() - lastmillis) > 50000){
    lastmillis = millis();
      if(ppm<=300){
        pumpNutriOn(2000);
         Serial.print("PPM Low turn On Pump for 10 ml ");
       }
       else if(ppm>=limitNutri){
        pumpNutriOff();
       }
     }
     yield();
   }
 
void pumpNutriOn(int start){
     analogWrite( MOTOR_A_PWM, LOW);
     analogWrite( MOTOR_B_PWM, LOW);
     delay(start);
     digitalWrite( MOTOR_A_PWM, HIGH );
     digitalWrite( MOTOR_B_PWM, HIGH );
      
   }
   void pumpNutriOff(){
     digitalWrite( MOTOR_A_PWM, HIGH );
     digitalWrite( MOTOR_B_PWM, HIGH );
    }



   
 /*************************************************************
 *                   Nutri Meter
 ************************************************************/ 

 void nutriLevel(){
    if((millis() - lastmillis) > 50000){
       lastmillis = millis();
  
  if(digitalRead(nutria == LOW) || digitalRead(nutrib == LOW)){
       nutrilevel = true;
       Serial.println("The Nutri tank is empty !" );
    }else{
       nutrilevel= false;
       Serial.println("The Nutri tank is empty !" );
      }
      }
    yield();
  }

/*************************************************************
 *                Water Meter
 ************************************************************/
void waterLevel(){
  // Transmitting pulse
  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(ECHO, INPUT);
  duration = pulseIn(ECHO, HIGH);
  // convert the time into a distance
   distance = (duration/2) / 29.1;//c(Speed of sound) = 331.5 + 0.6 * [air temperature in degrees Celsius]-->@ 20 degree c 343.5m/s---> converts to cm/microsecond -->0.03435 cm/us >> To facilitate the calculations>> Pace of sound = 1/speed of sound --> 1/0.03435 = 29.1
   //  Water tank level heigth 16 cm max. Convert distance to percent 
   percent =  100-((distance /16 )*100);
  // Serial.println("Distance ");
//Serial.println(distance);
 //  Serial.println((int)percent);
 // if(percent >=100){
    // Serial.println("Stop !! Water level hight");
   // }else if(percent<=25 ){
     //   Serial.println("Warning !! Water level low");
    //  }
  delay(1000);
  
  }



