
/*Copyright Dmitry Ivan 2017

MIT License
The MIT License (MIT)

Copyright (c) Dmitry Ivan <dmitryivan01@mail.ru> 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#define WIFI_CONNECTION_TIMEOUT_MS 15000
#define IP_ADDRESS_TIMEOUT_MS 30000
#define SMART_CONFIG_WAIT_MS 60000
    bool use_led_ = true;
    bool network_connected(){
        return WiFi.status() == WL_CONNECTED && !(WiFi.localIP() == INADDR_NONE);
    }

    bool connect_network(){
        // turn off led
        if(use_led_) {
            digitalWrite(BUILTIN_LED, HIGH);
        }

        // try to connect to the last known Wifi Network
        if(WiFi.SSID()!=NULL){
           Serial.println("Trying to connect to the last known network: " + WiFi.SSID());
            long wifi_timeout = millis();
            WiFi.begin(WiFi.SSID().c_str(), WiFi.psk().c_str());
            while(WiFi.status() != WL_CONNECTED && (millis() - wifi_timeout < WIFI_CONNECTION_TIMEOUT_MS)) {
                yield();
            }
            #ifdef _DEBUG_
                if(WiFi.status() != WL_CONNECTED){
                    Serial.println("Cannot connect to network:   " + WiFi.SSID());
                }
            #endif
        }

        // if not success connection then start the SmartConfig
        if(WiFi.status() != WL_CONNECTED){
            long wifi_timeout = millis();
            Serial.println( "Waiting Smart Config...");
            WiFi.stopSmartConfig();
            WiFi.beginSmartConfig();
            while(!WiFi.smartConfigDone()) {
                // stop smart config process on timeout
                if(millis() - wifi_timeout > SMART_CONFIG_WAIT_MS){
                    return false;
                }
                // blink led if required, otherwise yield to do network tasks
                if(use_led_){
                    digitalWrite(BUILTIN_LED, !digitalRead(BUILTIN_LED));
                    delay(500);
                }else{
                    yield();
                }
            }
            Serial.print( "Smart Config Process Completed!");
        }

        // ensure that the the ESP8266 get connected to the network
        long wifi_timeout = millis();
        while(WiFi.status() != WL_CONNECTED) {
            if(millis() - wifi_timeout > WIFI_CONNECTION_TIMEOUT_MS){
                Serial.println("Cannot connect to WiFi! Check the credentials!");
                // clear wifi configuration
                WiFi.disconnect();
                return false;
            }
            yield();
        }

        Serial.println( "Connected to WiFi!");
        // 10 small blinks to notify network connection
        if(use_led_) {
            for (int i = 0; i < 10; i++) {
                digitalWrite(BUILTIN_LED, !digitalRead(BUILTIN_LED));
                delay(100);
            }
        }

       Serial.println( "Getting IP Address...");
        // wait for an ip address
        wifi_timeout = millis();
        while (WiFi.localIP() == INADDR_NONE) {
            if(millis() - wifi_timeout > IP_ADDRESS_TIMEOUT_MS) return false;
            yield();
        }
        Serial.println("Got IP Address: ");
        Serial.println(WiFi.localIP());
        Serial.println("MAC Adress: ");
        Serial.print(WiFi.macAddress());
        
        return true;
    }

