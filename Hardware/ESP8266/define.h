/*MIT License
The MIT License (MIT)

Copyright (c) Dmitry Ivan <dmitryivan01@mail.ru> 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


int temp_water;
int ppm;
int lux;
bool nutrilevel ;
int nutrilimit;
int temp_inair;
int humi_inair;
int waterlevel;
bool swater;
String CLIENT_REGISTRATION_ID;
unsigned long lastmillis = 0;
//Define firebase file json (Realtime Database)
uint32_t chipid = ESP.getChipId();
String deviceid ="bionic/device/0/";
String _id = deviceid + "id";
String _sensor = deviceid + "sensor/";
String _switch = deviceid + "switch/";
String _setting = deviceid + "setting/";



// Config Firebase
#define FIREBASE_HOST "bionicgarden-afbdc.firebaseio.com"
#define FIREBASE_AUTH "Api key"
#define SERVER_KEY "key_from_dashboard"
//#define CLIENT_REGISTRATION_ID "key_from_client_after_registration"

