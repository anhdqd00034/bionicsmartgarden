/*Copyright Dmitry Ivan 2017

MIT License
The MIT License (MIT)

Copyright (c) Dmitry Ivan <dmitryivan01@mail.ru> 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE
*/

#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <FirebaseCloudMessaging.h>
#include <SoftwareSerial.h>
#include <SerialCommand.h>
  #include <WiFiUdp.h>
//#include "init_wifi.h"
#include "define.h"
// virtual serial port
const byte RX = 13;
const byte TX = 15;
 
SoftwareSerial mySerial = SoftwareSerial(RX, TX, false, 256); 
SerialCommand sCmd(mySerial);

 const int pumpPin = 12; 
 const int ledPin = 14;
 void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  network_connected();
  connect_network();
  sCmd.addCommand("SENTDATA", getDataArduino);
  // sCmd.addDefaultHandler(getDataArduino);
  pinMode(BUILTIN_LED, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(pumpPin, OUTPUT);
  digitalWrite(pumpPin, LOW);
  digitalWrite(ledPin,LOW);
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void loop() {
    pushDataFireBase();
    switchControl();
    sCmd.readSerial();
    sentNoti();
}
 
  void getDataArduino(){
     #ifdef PROGMEM
  char *json = sCmd.next(); 
  //Serial.println(json);
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(json);
   JsonArray& nestedArray = root[F("data")];
   JsonArray& nestedArray2 = root[F("set")];
  if (!root.success()) {
    Serial.println(" ParseObject() failed");
    return;
  }
      lux = nestedArray[0];         // light sensor
      ppm = nestedArray[1];         // tds sensor
      temp_water= nestedArray[2]; // temp in water
      temp_inair = nestedArray[3] ; // temp in air
      humi_inair =  nestedArray[4]; // humidity in air
      waterlevel= nestedArray[5]; // water level
      nutrilevel = nestedArray[6]; // nutri (true is has)
      nutrilimit = nestedArray2[0];  // nutri limit
      //Debug
     root.printTo(Serial);
       #else
    #warning PROGMEM is not supported on this platform
    #endif
    }
  void sentDataArduino(){ 
  #ifdef PROGMEM
   DynamicJsonBuffer jsonBuffer;
   JsonObject& root = jsonBuffer.createObject(); 
    root[F("swater")]= swater;       
    mySerial.print(F("SENTDATA"));
    mySerial.print('\r');
    root.printTo(mySerial);
    mySerial.print('\r');
    #else
    #warning PROGMEM is not supported on this platform
    #endif
      }

    void switchControl(){
       digitalWrite(ledPin, Firebase.getBool(_switch + "led") ? HIGH : LOW);
       //bool pumpValue = Firebase.getBool(_switch + "pump");
       digitalWrite(pumpPin, Firebase.getBool(_switch + "pump") ? HIGH : LOW);
       // Serial.print("Pump :" + pumpValue);
     
      }

    
  
    void pushDataFireBase(){
       if((millis() - lastmillis) > 5000){
        lastmillis = millis();
      Firebase.setInt( _sensor + "tempinair",temp_inair);
      Firebase.setInt( _sensor + "tempinwater",temp_water);
      Firebase.setInt(_sensor + "humidity",humi_inair);
      Firebase.setInt(_sensor + "light",lux );
      Firebase.setInt(_sensor + "tds",ppm);
      Firebase.setInt(_sensor + "waterlevel",waterlevel);
      Firebase.setBool(_sensor + "nutrilevel",nutrilevel);
      Firebase.setInt(_setting + "nutri",nutrilimit);
      if (Firebase.failed()) {
      Serial.print("Update Data Error !!!");
      Serial.println(Firebase.error());  
      return;
          }
        }
      }

  void sentNoti(){
    if(ppm<300){
       sentMessaging("Warning !!","PPM Low ! Automatic pump nutri active");
      }
    else if(waterlevel<20){
        sentMessaging("Warning !!","Water Low ! Automatic Pump Water Active");
         Firebase.setBool(_switch + "swater",true);
         sentDataArduino();
         if(waterlevel>=100){
          Firebase.setBool(_switch + "swater",false);
          sentDataArduino();
          }
      }
      else if(temp_inair > 40){
        sentMessaging("Warning !!","Temp hot ! Need AirCondition");
        
        }
    }
      

  void sentMessaging(const std::string& title , const std::string& content){
  FirebaseCloudMessaging fcm(SERVER_KEY);
  FirebaseCloudMessage message =
  FirebaseCloudMessage::SimpleNotification(title,content);
  // If User register user account in android app sent token key server
  // Device get token key message and sent message to android app
  Firebase.getString(deviceid+"token",CLIENT_REGISTRATION_ID);
  FirebaseError error = fcm.SendMessageToUser(CLIENT_REGISTRATION_ID, message);
  if (error) {
    Serial.print("Error:");
    Serial.print(error.code());
    Serial.print(" :: ");
    Serial.println(error.message().c_str());
  } else {
    Serial.println("Sent OK!");
  }
   }

      

